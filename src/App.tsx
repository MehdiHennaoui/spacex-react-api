import React, {Component, useEffect} from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  const [launches, setLaunches] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  useEffect(() => {
    setIsLoading(true)
    fetch('https://api.spacexdata.com/v3/launches')
      .then(response => {
        return response.json()
      })
      .then(data => setLaunches(data))
      .then(() => setIsLoading(false))
      .catch(error => setIsLoading(false))
  }, []);

  console.log("launches", launches.length);

  return (
    <div className="">
      <h1 className="text-2xl mb-3 text-center ">Lancement spaces X</h1>
      <div className={"grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 p-4 gap-4"}>
        {!isLoading && launches.length > 0 ?
          launches.slice(0, 10).map((launch, index) => {
            return <Card key={index} launch={launch} />
        }) : <div className={"text-center"}>Chargement ...</div>}
      </div>
    </div>
  );
}


type launchTypes = {
  flight_number: number;
  mission_name: string;
  launch_date_local: string;
  launch_success: boolean;
  rocket: {
    rocket_name: string;
    rocket_type: string;
  };
  links: {
    mission_patch_small: string;
  };
};

function formatDate(date: string) {
  const dateObj = new Date(date);
  const options = {
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  };
  // @ts-ignore
  return dateObj.toLocaleDateString('fr-FR', options);
}

const Card: ({launch}: { launch: launchTypes }) => JSX.Element = ({launch}) => {

  return (
    <div className="shadow-lg p-4">
        <h2 className="text-xl mb-4">{launch.mission_name}</h2>
        <p className="text-bold mb-2">{formatDate(launch.launch_date_local)}</p>
        <p className="mb-2">{launch.rocket.rocket_name}</p>
        <a href={launch.links.mission_patch_small} className="underline decoration-2 text-blue-500">Lien image</a>
    </div>
  );
};

export default App;
